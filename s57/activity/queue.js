let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    let result = [];
      let index = 0;
      while (collection[index] !== undefined) {
        result[index] = collection[index];
        index++;
      }
      return result;
}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
   let newCollection = [];
     let index = 0;
     while (collection[index] !== undefined) {
       newCollection[index] = collection[index];
       index++;
     }
     newCollection[index] = element;
     collection = newCollection;
     return collection;
}

function dequeue() {
    // In here you are going to remove the first element in the array
    let newCollection = [];
      for (let i = 1; i < collection.length; i++) {
        newCollection[i - 1] = collection[i];
      }
      collection = newCollection;
      return collection;
}

function front() {
    // you will get the first element
    if (collection[0] === undefined) {
        return null;
      }
      return collection[0];
}

function size() {
     // Number of elements  
     let count = 0;
       for (let item of collection) {
         count++;
       }
       return count;
}

function isEmpty() {
    //it will check whether the function is empty or not
    let empty = true;
      for (let item of collection) {
        empty = false;
        break;
      }
      return empty;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
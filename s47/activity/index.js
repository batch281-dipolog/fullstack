const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

const updateName = () => {
    const firstName = txtFirstName.value;
    const lastName = txtLastName.value;
    const fullName = firstName + ' ' + lastName;
    spanFullName.innerHTML = fullName;
}

txtFirstName.addEventListener('keyup', updateName);
txtLastName.addEventListener('keyup', updateName);
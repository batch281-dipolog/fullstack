// console.log("Hello World!");

// Get Post Data
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data));

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method : 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('txt-body').value,
			userId: 1
		}),
		headers: {'Contect-type' : 'application/json; charcet=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});
});

// Show Posts
const showPosts = (posts) => {
	let postEntries = '';

	// We are using the forEach method to iterate every post in our website. So that the website will show our created posts one by one.
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
	document.querySelector(`#btn-submit-update`).removeAttribute('disabled');
}


// Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id : document.querySelector('#txt-edit-id').value,
			title : document.querySelector('#txt-edit-title').value,
			body : document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully Updated')

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})

})

// Activity s49 Code
// Delete Post
const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: 'DELETE'
    })
    .then((response) => {
        if (response.ok) {
            document.querySelector(`#post-${id}`).remove();
            alert('Post deleted successfully');
        } else {
            console.error('Failed to delete the post');
            alert('An error occurred while deleting the post');
        }
    })
    .catch(() => alert('An error occurred while deleting the post'));
};
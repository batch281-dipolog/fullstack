// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import {Button, Form, Row, Col} from 'react-bootstrap';
//we need to import the useState from the react
import {useState, useEffect, useContext} from 'react';
import { Navigate, useNavigate } from 'react-router-dom'; // Activity s54

import UserContext from '../UserContext';
import Swal3 from 'sweetalert2';

export default function Register(){

    const navigate = useNavigate(); // Activity s54

    //State hooks to store the values of the input fields
    const [firstname, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isPassed, setIsPassed] = useState(true);

    const [isDisabled, setIsDisabled] = useState(true);

    //we are going to add/create a state that will declare whether the password1 and password 2 is equal
    const [isPasswordMatch, setIsPasswordMatch] = useState(true); 
    // Activity s54
    //Allows us to consume the UserContext object and it's properties to use for user validation
    const { user } = useContext(UserContext);

 
    //when the email changes it will have a side effect that will console its value
    useEffect(()=>{
        if(email.length > 15){
            setIsPassed(false);
        }else{
            setIsPassed(true);
        }
    }, [email]);

    // This useEffect will disable or enable the sign-up button
    useEffect(() => {
      // Check if all the required fields are filled and the passwords match
      if (
        email !== '' &&
        password1 !== '' &&
        password2 !== '' &&
        password1 === password2 &&
        email.length <= 15 &&
        firstname.length <= 15 &&
        lastname.length <= 15
      ) {
        setIsDisabled(false);
      } else {
        setIsDisabled(true);
      }
    }, [email, password1, password2, firstname, lastname]);

    
    //function to simulate user registration
    function registerUser(event) {
        //prevent page reloading
        event.preventDefault();

        // Activity 55
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify({
                email: email
              })
            })
              .then((response) => response.json())
              .then((data) => {
                if (data === false) {

                    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                          method: 'POST',
                          headers: { 'Content-Type': 'application/json' },
                          body: JSON.stringify({
                            firstName: firstname,
                            lastName: lastname,
                            email: email,
                            password: password1
                          })
                        })
                          .then((response) => response.json())
                          .then((data) => {
                            if (data === true) {
                              Swal3.fire({
                                title: 'Registration Successful',
                                icon: 'success',
                                text: 'Thank you for registering!'
                              }).then(() => {
                                navigate('/login');
                              });
                            } else {
                              Swal3.fire({
                                title: 'Registration Failed',
                                icon: 'error',
                                text: 'Please try again.'
                              });
                            }
                          });

                      
                } else {
                    Swal3.fire({
                      title: 'User Exist',
                      icon: 'error',
                      text: 'Please make anothe email.'
                    })
                }
            })
        }

    //useEffect to validate whether the password1 is equal to password2
    useEffect(() => {
        if(password1 !== password2){
            setIsPasswordMatch(false);
        }else{
            setIsPasswordMatch(true);
        }

    }, [password1, password2]);

    return(
        user.id === null || user.id === undefined
        ?
            <Row>
                <Col className = "col-6 mx-auto">
                    <h1 className = "text-center">Register</h1>
                    <Form onSubmit ={event => registerUser(event)}>

                          <Form.Group className="mb-3" controlId="formBasicFirstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control 
                                type="firstname" 
                                placeholder="Enter firstname" 
                                value = {firstname}
                                onChange = {event => setFirstName(event.target.value)}
                                />
                            <Form.Text className="text-danger" hidden = {isPassed}>
                              The firstname should not exceed 15 characters!
                            </Form.Text>
                          </Form.Group>

                          <Form.Group className="mb-3" controlId="formBasicLastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control 
                                type="lastname" 
                                placeholder="Enter lastname" 
                                value = {lastname}
                                onChange = {event => setLastName(event.target.value)}
                                />
                            <Form.Text className="text-danger" hidden = {isPassed}>
                              The lastname should not exceed 15 characters!
                            </Form.Text>
                          </Form.Group>

                          <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email" 
                                value = {email}
                                onChange = {event => setEmail(event.target.value)}
                                />
                            <Form.Text className="text-danger" hidden = {isPassed}>
                              The email should not exceed 15 characters!
                            </Form.Text>
                          </Form.Group>

                          <Form.Group className="mb-3" controlId="formBasicPassword1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                value = {password1}
                                onChange = {event => setPassword1(event.target.value)}
                                />
                          </Form.Group>

                          <Form.Group className="mb-3" controlId="formBasicPassword2">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Retype your nominated password" 
                                value = {password2}
                                onChange = {event => setPassword2(event.target.value)}
                                />

                            <Form.Text className="text-danger" hidden = {isPasswordMatch}>
                              The passwords does not match!
                            </Form.Text>
                          </Form.Group>

                          

                          <Button variant="primary" type="submit" disabled = {isDisabled}>
                            Sign up
                          </Button>
                    </Form>
                </Col>
            </Row>

        :
            <Navigate to = '*' />
        )
}
